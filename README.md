# CyberGun_Airsoft

## Dependencias

### ARM Toolchain
- Download [`ARM Embedded Toolchain`](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)

- Descompacte o arquivo do toolchain em uma pasta de sua preferencia:

`tar -xf gcc-arm-none-eabi-<version>.tar.bz2 -C <output_path>`

- Adicione o conjunto de ferramentas **gcc-arm-none-eabi** no no PATH do sistema

`export PATH=$PATH:<output_path>/gcc-arm-none-eabi-<version>/bin`

- Valide se a instalação foi feita com sucesso:

`arm-none-eabi-gcc --version`

`arm-none-eabi-gdb --version`

- Caso alguma das ferramentas acima retornar um erro ao executar o comando para retornar a versão, verifique se não existe nenhuma dependencia de bibliteca. Caso exista, instale as bibliotecas necessárias.

`ldd arm-none-eabi-gcc`

`ldd arm-none-eabi-gdb`


### ST_Linker

- Instale a ferramenta stlink através do gerenciador de pacotes do Linux:

`sudo apt install stlink-tools`

